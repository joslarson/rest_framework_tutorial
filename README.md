# Django Rest Framework Tutorial

(This tutorial is a lightly modified version of [this](http://www.django-rest-framework.org/tutorial/quickstart/))

We're going to create a simple API to allow admin users to view and edit the users and groups in the system.

## Project setup

Get the stuff!

    # Create the project directory
    git clone https://joelarson@bitbucket.org/joelarson/rest_framework_tutorial.git
    cd rest_framework_tutorial

    # Install Django and Django REST framework (and a few other things)
    pip install -r requirements.txt

Now sync your database for the first time:

    python manage.py migrate

We'll also create an initial user named `admin` with a password of `password`. We'll authenticate as that user later in our example.

    python manage.py createsuperuser

Once you've set up a database and initial user created and ready to go, open up the app's directory and we'll get coding...


## Settings

We'd also like to set a few global settings.  We'd like to turn on pagination, and we want our API to only be accessible to admin users via basic auth.  The settings module will be in `tutorial/settings.py`

```python
INSTALLED_APPS = (
    ...
    'rest_framework',
)

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.IsAdminUser'
    ],
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.BasicAuthentication',
    ),
    'DEFAULT_FILTER_BACKENDS': ('rest_framework.filters.DjangoFilterBackend',),
    'PAGINATE_BY': 10,                 # Default to 10
    'PAGINATE_BY_PARAM': 'page_size',  # Allow client to override, using `?page_size=xxx`.
    'MAX_PAGINATE_BY': 100             # Maximum limit allowed when using `?page_size=xxx`.
}
```

## Serializers

First up we're going to define some serializers. Let's create a new module named `tutorial/serializers.py` that we'll use for our data representations.

```python
from django.contrib.auth.models import User, Group
from rest_framework import serializers


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups')


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')
```

Notice that we're using hyperlinked relations in this case, with `HyperlinkedModelSerializer`.  You can also use primary key and various other relationships, but hyperlinking is good RESTful design.


## Views

Right, we'd better write some views then.  Open `tutorial/views.py` and get typing.

```python
from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from tutorial.serializers import UserSerializer, GroupSerializer


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
```

Rather than write multiple views we're grouping together all the common behavior into classes called `ViewSets`.

We can easily break these down into individual views if we need to, but using viewsets keeps the view logic nicely organized as well as being very concise.

For trivial cases you can simply set a `model` attribute on the `ViewSet` class and the serializer and queryset will be automatically generated for you.  Setting the `queryset` and/or `serializer_class` attributes gives you more explicit control of the API behaviour, and is the recommended style for most applications.


## URLs

Okay, now let's wire up the API URLs.  On to `tutorial/urls.py`...

```python
from django.conf.urls import url, include
from rest_framework import routers
from tutorial import views

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url(r'^', include(router.urls)),
    ...
]
```

Because we're using viewsets instead of views, we can automatically generate the URL conf for our API, by simply registering the viewsets with a router class.

Again, if we need more control over the API URLs we can simply drop down to using regular class based views, and writing the URL conf explicitly.

Finally, we're including default login and logout views for use with the browsable API.  That's optional, but useful if your API requires authentication and you want to use the browsable API.

----

## Testing our API

Fire up your django testing server with `python manage.py runserver`, and head to [http://127.0.0.1:8080](http://127.0.0.1:8080). Login via BasicAuth with the credentials you setup earlier, create some groups, then create some users with attached groups.